import { Document, Model, model, Schema } from 'mongoose'

export interface ITransaction extends Document {
  id: string
  amount: number
  senderId: string
  receiverId: string
  currency: string
  createdAt: Date
}

// TODO: add type validation
const TransacationSchema: Schema = new Schema({
  id: {
    type: Schema.Types.ObjectId,
    required: true,
  },
  amount: {
    type: Number,
    required: true,
  },
  senderId: {
    type: String,
    required: true,
  },
  receiverId: {
    type: String,
    required: true,
  },
  currency: {
    type: String,
    required: true,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
})

export const Transaction: Model<ITransaction> = model<ITransaction>(
  'Book',
  TransacationSchema
)
