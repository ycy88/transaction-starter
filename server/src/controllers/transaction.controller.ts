import { Request, Response } from 'express'
import { ITransaction, Transaction } from '../models/transaction.model'
import { Types } from 'mongoose'

export default class TransactionController {
  public async index(req: Request, res: Response) {
    // TODO: try catch logic
    // TODO: filter unneed fields from transactions
    const transactions = await Transaction.find({})
    res.json({ transactions })
  }

  public async create(req: Request, res: Response) {
    try {
      const transactionDto: ITransaction = {
        ...req.body,
        id: new Types.ObjectId(),
      }
      await Transaction.create(transactionDto)

      return res.json({ msg: 'Success' })
    } catch (error) {
      console.error(error)
      return res.json({ msg: 'Error' }).status(500)
    }
  }
}

export const transactionController = new TransactionController()
