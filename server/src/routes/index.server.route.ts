import { Express } from 'express'
import { transactionController } from '../controllers/transaction.controller'

export default class TransactionRoute {
  constructor(app: Express) {
    app.route('/transactions').get(transactionController.index)
    app.route('/transactions').post(transactionController.create)
  }
}
