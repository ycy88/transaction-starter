import * as bodyParser from 'body-parser'
import * as cors from 'cors'
import * as express from 'express'
import * as helmet from 'helmet'
import * as mongoose from 'mongoose'
import * as logger from 'morgan'
import * as path from 'path'

import {
  DB_CONNECTION_STRING,
  ROUTES_DIR,
  MODELS_DIR,
  USE_DB,
  PORT,
} from './var/config'
import { globFiles } from './helpers'

const app: express.Express = express()

for (const model of globFiles(MODELS_DIR)) {
  require(path.resolve(model))
}

if (USE_DB) {
  mongoose
    .connect(DB_CONNECTION_STRING, {
      promiseLibrary: global.Promise,
    })
    .catch(() => {
      console.log('Error connecting to mongo')
    })
}

app.use(logger('dev'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

app.use(helmet())
app.use(cors())

for (const route of globFiles(ROUTES_DIR)) {
  require(path.resolve(route)).default(app)
}

app.listen(PORT, () => {
  if (USE_DB) {
    console.log(
      `Server started on port ${PORT} on env ${process.env.NODE_ENV ||
        'dev'} dbcon ${DB_CONNECTION_STRING}`
    )
  } else {
    console.log(
      `Server started on port ${PORT} on env ${process.env.NODE_ENV || 'dev'}`
    )
  }
})
