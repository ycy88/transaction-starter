import React, { useState } from 'react';
import './App.css';

import axios from 'axios';
import { useGetTransactions, ITransaction } from './hooks/transactions';
import { API_URL } from './config';

const App: React.FC = () => {
	const [
		//@ts-ignore // TS does not like mixing types in array
		{ transactions, isLoading, isError },
		setTransactionAdded
	] = useGetTransactions();
	const [amount, setAmount] = useState(0);

	const sendBitcoinToCy = async (amount: number) => {
		try {
			await axios.post(API_URL, {
				receiverId: 'cy6581',
				senderId: 'user123',
				currency: 'Bitcoin',
				amount: amount
			});
			//@ts-ignore // TS does not like mixing types in array
			setTransactionAdded(Date.now());
		} catch (error) {
			console.error('Failed to send money');
		}
	};

	return (
		<div className="App">
			<header className="App-header">
				<p>Transaction List</p>
			</header>
			<section className="App-Body">
				{isError && <div>Something went wrong ...</div>}

				{isLoading ? (
					<div>Loading ...</div>
				) : transactions.length ? (
					<table className="App-table">
						<thead>
							<tr>
								<th>Sender</th>
								<th>Receiver</th>
								<th>Currency</th>
								<th>Amount</th>
								<th>Date</th>
							</tr>
						</thead>
						<tbody>
							{transactions.map((t: ITransaction) => (
								<tr key={t.id}>
									<td>{t.senderId}</td>
									<td>{t.receiverId}</td>
									<td>{t.currency}</td>
									<td>{t.amount}</td>
									<td>{new Date(t.createdAt).toLocaleDateString()}</td>
								</tr>
							))}
						</tbody>
					</table>
				) : (
					'No transactions found'
				)}
			</section>
			<br></br>
			<br></br>
			<section>
				<strong>Send some Bitcoin to CY</strong>
				<form
					onSubmit={event => {
						event.preventDefault();
						sendBitcoinToCy(amount);
					}}
				>
					<input
						type="number"
						value={amount}
						onChange={event => setAmount(parseInt(event.target.value))}
						min="1"
						max="50"
					/>
					<button type="submit">Send !</button>
				</form>
			</section>
		</div>
	);
};

export default App;
