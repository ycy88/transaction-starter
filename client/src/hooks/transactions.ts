import { useState, useEffect } from 'react';
import axios from 'axios';
import { API_URL } from '../config';

export interface ITransaction {
	id?: string;
	amount: number;
	senderId: string;
	receiverId: string;
	currency: string;
	createdAt: Date;
}

export const useGetTransactions = () => {
	const [transactions, setTransactions] = useState<ITransaction[]>([]);
	const [newTransaction, setNewTransaction] = useState(Date.now());
	const [isLoading, setIsLoading] = useState(false);
	const [isError, setIsError] = useState(false);

	useEffect(() => {
		const fetchData = async () => {
			setIsError(false);
			setIsLoading(true);
			try {
				const result = await axios.get(API_URL);
				const transactions = result.data.transactions;
				setTransactions(transactions);
			} catch (error) {
				setIsError(true);
			}
			setIsLoading(false);
		};
		fetchData();
	}, [newTransaction]);

	return [{ transactions, isLoading, isError }, setNewTransaction];
};
