Adapted from

- https://github.com/rjmacarthy/express-typescript-starter
- https://github.com/brijeshbhakta30/node-express-mongoose-starter

# 1 - Setup Express Server

Ensure you have Docker installed

Ensure you are in the server folder

```
cd server
```

Build the image

```
npm run build:docker
```

Run the image

```
npm run docker:compose
```

Exposed Endpoints:
GET http://localhost:8080/transactions
POST http://localhost:8080/transactions

# 2 - Serve React Project

Ensure you are in the client folder

```
cd client
```

Install dependencies

```
npm install
```

Start the React project

```
npm run start
```

Browse to the home page at https://localhost:3000

# License

MIT
